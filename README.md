Visualizing a Keras Model with Tensorboard

1. create a folder and place keras_model_to_tf.py into it

2. run keras_model_to_tf.py. It should place the graph information required for tensorboard into ./logs_for_tb

3. in the terminal navigate to the folder where keras_model_to_tf.py is located

4. run tensorboard and give it the directory where the graph information is located:

	$ tensorboard –logdir=./logs_for_tb 
	(before logdir we have a double -)

5. open the url returned by tensorboard in browser

6. the model visualization should be located in the 'Graph' tab  

