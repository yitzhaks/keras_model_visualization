import tensorflow as tf
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense
import os

#create the log directory
# the information necessary for the tensorboard model visualization will
# be written there
LOGDIR = "logs_for_tb/"
if not os.path.exists(LOGDIR): os.makedirs(LOGDIR)
[os.remove(os.path.join(LOGDIR, f)) for f in os.listdir(LOGDIR)]

# define keras model
model = Sequential()
model.add(Dense(6, input_dim=8, init='uniform', activation='relu'))
model.add(Dense(4, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

#get TF session which contains the keras model
sess = K.get_session()

#write model visualization to disc
writer = tf.summary.FileWriter(LOGDIR)
tf_model=sess.graph
writer.add_graph(tf_model)
print('done !')